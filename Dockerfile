FROM python:3-stretch

ADD . /app
WORKDIR /app

CMD ["/usr/bin/python3", "/app/pi-led-flash.py"]
