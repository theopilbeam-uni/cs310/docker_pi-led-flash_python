#!/usr/bin/env python3

import atexit
import logging as log
from time import sleep
from sys import exit

log.basicConfig(
    level=log.DEBUG
)

LEDPATH = '/sys/class/leds/led0'
DEFTRIG = 'mmc0'

def write_brightness(b):
    try:
        with open('%s/brightness' % (LEDPATH), 'w') as file:
            file.write(b)
    except Exception as e:
        log.warning("Couldn't open brightness file")
        log.warning(e)
        exit(1)

def write_trigger(t):
    try:
        with open('%s/trigger' % (LEDPATH), 'w') as file:
            file.write(t)
    except Exception as e:
        log.warning("Couldn't open trigger file")
        log.warning(e)
        exit(1)

        
log.info('Setting trigger to `none`')
write_trigger('none')


go = True

log.info('Registering cleanup function')
def cleanup():
    go = False
    log.info('Restoring default trigger value')
    write_trigger(DEFTRIG)
    
atexit.register(cleanup)


log.info('Entering main loop')
while go:
    write_brightness('255')
    sleep(1)
    write_brightness('0')
    sleep(1)
